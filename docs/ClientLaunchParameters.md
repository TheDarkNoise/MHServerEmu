# Client Launch Parameters

To launch any version of the game without any launcher you can use the following combination of launch parameters: "-solidstate -nobitraider -nosteam". Older versions of the game from before BitRaider was removed don't require the -solidstate parameter.

## Game-specific parameters

These require further checking for compatibility with various versions of the client.

| Parameter              | Description |
| ---------------------- | ----------- |
| -defaultregion=        |             |
| -defaultcell=          |             |
| -startingregion=       |             |
| -startingcell=         |             |
| -envoverride=          |             |
| -collectionname=       |             |
| -emailaddress=         |             |
| -password=             |             |
| -twofactorauthcode=    |             |
| -twofactorauthname=    |             |
| -twofactortrustmachine |             |
| -loginasplayer         |             |
| -loginasanother        |             |
| -nosave                |             |
| -serveroverride=       |             |
| -webtoken              |             |
| -siteconfigurl=        |             |
| -nosolidstate          |             |
| -nosteam               |             |
| -robocopy              |             |
| -solidstate            |             |
| -steam                 |             |
| -skipmotioncomics      |             |
| -nocatalog             |             |
| -displaynametoggle     |             |
| -xpnumberstoggle       |             |
| -floatingnumberstoggle |             |
| -combatlogtoggle       |             |
| -nouinotifications     |             |

## Generic Unreal Engine parameters

| Parameter        | Description |
| ---------------- | ----------- |
| -nostartupmovies |             |
| -nomovies        |             |
| -nosplash        |             |
